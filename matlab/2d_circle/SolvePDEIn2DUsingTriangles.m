function SolvePDEIn2DUsingTriangles(paramsfile, Nr) 
    % read the parameters
    params        = readparameters(paramsfile)

    % @@@@@@@@@@@@@@@@@@@
    % @@@ create mesh @@@
    % @@@@@@@@@@@@@@@@@@@

    Mesh        = MakeMesh(params, Nr,1,1);
      
    % write a function that combines everything you learned to solve the PDE
    % using the mesh structure you are provided with
    u = solvePDE(params, Mesh);

    vtkfile=sprintf('%s_Linear%d_PDEs%d_mu%02d',file_prefix,params.Linear, ...
                                               params.PDEs_as_constraints,i)


    % write a function that uses writeMeshToVTKFile(...) called postprocessor
    % that takes as arguments the name of the file and the solution, and write
    % the vtkfile on the disk for visualization with ViSiT

    % postprocessor(vtkfile, u);
 
end

function U = u_exact(r, phi)

  n = length(r);
  U = zeros(n,1);
  for i=1:n

    if (r(i) <= 1.0)
      U(i) = 1 + r(i)*cos(phi(i));
    else
      U(i) = 1 + (1.5*r(i) - 0.5/r(i))*cos(phi(i));
    end

  end

end


function F = f(r, phi)

    % this is the restriction of u_exact on the boundary
    F = 1 + 11/4 * cos(phi);

end




function G = g(r, phi)

    % this is the Neumann data
    % -grad(u) * n = g
    G = -13.0/8.0 * cos(phi);

end






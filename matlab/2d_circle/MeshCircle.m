function [Mesh] = MeshCircle(params, nr, a, b)

    r0                 = 0.0;
    r1                 = 2.0;
    dr                 = (r1-r0)/nr;
    [tri, R, Phi]      = generateTriangulation(params, r0, r1, nr);

    dimension          = 2;
    NumberOfElements   = tri.size(1);
    NumberOfVertices   = tri.size(2);
    Elements           = tri.ConnectivityList;
    Points             = tri.Points;
    NumberOfNodes      = size(Points, 1);
    PointMarkers       = zeros(NumberOfNodes,       1);
    CondactivityE      = zeros(NumberOfElements,    1);
    alpha_0            = zeros(NumberOfElements,    1);
    CondactivityN      = zeros(NumberOfNodes,       1);
    type               = 5;
    edges              = tri.edges;
    edge_tri           = edgeAttachments(tri, edges);

    cellsz             = cellfun('length',edge_tri);
    Ii                 = find(cellsz == 2); % indices of interior edges
    Ib                 = find(cellsz == 1); % indices of boundary
                                            % edges

    Edges_i            = edge_tri(Ii); Edges_i = [Edges_i{:}];
    Edges_i            = reshape(Edges_i, 2, length(Edges_i)/2)';
    Edges_b            = edge_tri(Ib); Edges_b = [Edges_b{:}]';

    NumberOfEdges      = size(Edges_i, 1);

    for i=1:NumberOfNodes
      p = Points(i,:);
      r = norm(p);
      if ( abs(r - 2.0) < 1e-12 )
        PointMarkers(i)  = 1;
      else
        PointMarker(i)   = 0;
      end

      if ( norm(r) <= 1.0+1e-10 )
        CondactivityN(i) = 2;
      else
        CondactivityN(i) = 1;
      end

    end


    OuterDiskTriangles = [];
    InnerDiskTriangles = [];

    for e=1:NumberOfElements
      I = Elements(e,:); % element nodes
      p = Points(I,:);   % coordinates of element nodes
      c = mean(p);       % coordinates of the center of the element
      cx = c(1); cy = c(2);
      if ( cx^2/a^2 + cy^2/b^2  < 1.0 )
        InnerDiskTriangles = [InnerDiskTriangles e];
        CondactivityE(e) = 2;
        alpha_0(e)       = 2;
      else
        OuterDiskTriangles = [OuterDiskTriangles e];
        CondactivityE(e) = 1;
        alpha_0(e)       = 1;
      end


    end



    for e=1:NumberOfElements
      I = Elements(e, :);
      EleBoundary(e,1) = sum(PointMarkers(I));
    end 
    
    BoundaryElements = find(EleBoundary > 0);

    % 3
    % |\
    % | \
    % |__\
    % 1   2
    local_edges = [2 3; ...
                   3 1; ...
                   1 2];

    bel = length(Edges_b);
    BoundaryEdges = zeros(bel, 2);
    B_N = sparse(NumberOfNodes, NumberOfNodes);

    edges              = tri.edges;
    markers            = PointMarkers(edges);

    markers            = sum(markers');
    I                  = find(markers == 2);

    nboundary_edges    = length(I);
    for e = 1:nboundary_edges

      v   = edges(I(e), :);
      P   = Points(v, :);
      l   = norm(P(2,:) - P(1,:));

      B_N(v, v) = B_N(v, v) + l*[1/3 1/6; ...
                                1/6 1/3];
    end 


    fprintf('mesh::makeRectilinearGrid constructing mesh::Points ...\n');


    I    = find(PointMarkers == 1);
    B_S  = B_N(I, I);

    Mesh = struct('Nr',               nr,               ... 
                  'h',                dr,               ...
                  'SemiAxes',         [a b],            ...
                  'NumberOfElements', NumberOfElements, ...
                  'NumberOfNodes',    NumberOfNodes,    ...
                  'NumberOfVertices', NumberOfVertices, ...
                  'NumberOfEdges',    NumberOfEdges,    ...
                  'Elements',         Elements,         ...
                  'Edges',            tri.edges,        ...
                  'BoundaryElements', BoundaryElements, ...
                  'BoundaryEdges',    BoundaryEdges,    ...
                  'Edges_b',          Edges_b,          ...
                  'Edges_i',          Edges_i,          ...
                  'Points',           Points,           ...
                  'RPhi',             [R(:) Phi(:)],    ...
                  'PointMarkers',     PointMarkers,     ...
                  'DirichletIndex',   I,                ...
                  'CondactivityE',    CondactivityE,    ...
                  'CondactivityN',    CondactivityN,    ...
                  'alpha_0',          alpha_0,          ...
                  'B_N',              B_N,              ...
                  'InnerDiskTriangles', InnerDiskTriangles, ...
                  'OuterDiskTriangles', OuterDiskTriangles, ...
                  'VTKtype',          type);

    fprintf('constructing mesh::Elements.. %d\n', NumberOfElements);
    fprintf('constructing mesh::Points ... %d\n', NumberOfNodes);
end  % makeMesh


function [tri, R, Phi] = generateTriangulation(params, r0, r1, nr)

    dr   = (r1-r0)/nr;
    phi  =[];
    r    =[];

    for i=0:nr/2
      r_i   = r0 + i*dr;
      
      dphi = dr / r_i;  
      
      phi_i = 0:dphi:2*pi;
    
      m=length(phi_i);
      rsi = repmat(r_i, 1, m);
      phi = [phi phi_i];
      r   = [r rsi];
    end
 
    if (params.Linear)
       r_i   = 1+params.mesh_perturbation;
       dphi = dr;  

       phi_i = 0:dphi:2*pi;
       m=length(phi_i);
       rsi = repmat(r_i, 1, m);
       phi = [phi phi_i];
       r   = [r rsi];
    end  

    for i=nr/2+1:nr
       r_i   = r0 + i*dr;
 
       dphi = dr / r_i;  
 
       phi_i = 0:dphi:2*pi;
 
       m=length(phi_i);
       rsi = repmat(r_i, 1, m);
       phi = [phi phi_i];
       r   = [r rsi];
    end
 
    R=r;
    Phi=phi;
    
    
    X  = R.*cos(Phi);
    Y  = R.*sin(Phi);
    X=X(:); Y=Y(:);
    tri                = delaunayTriangulation(X,Y);
end

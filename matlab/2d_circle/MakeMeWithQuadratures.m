function [Me] = MakeMeWithQuadratures(e,mesh)
% Quadrature points
xi  = [4; 1; 1]/6;
eta = [1; 4; 1]/6;

% Quadrature weights
W   = [1; 1; 1]/6;

%number of Gauss points
NGp = size(xi,1);

Nv = mesh.NumberOfVertices;
Me = zeros(Nv,Nv);

% Compute Jacobian of the transformation
J = ...
    
% Compute the absolute value of the determinant of the Jacobian
detJ = ...
    
% loop over all quadrature points
for k=1:NGp
    % evaluate the basis function at the Quadrature points
    % basis functions are
    N = [1-xi(k)-eta(k); xi(k); eta(k)];
    
    % Compute the mass matrix entries over all the Quadrature points
    for i=1:Nv
        for j=1:Nv
            Me(i,j)  = ...
        end % j loop
    end % i loop
end % k loop
end % function

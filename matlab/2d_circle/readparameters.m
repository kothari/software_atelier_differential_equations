  function s = readparameters(filename)

  fid = fopen(filename);
  C = textscan(fid, '%s%s');
  fclose(fid); 

  linear              = yesORno(C{2}(1));
  i0                  = str2double(C{2}(2));
  L2misfit_coef       = str2double(C{2}(3));
  mesh_perturbation   = str2double(C{2}(4));

  s = struct('Linear',                     linear,              ...
             'i0',                         i0,                  ...
             'L2misfit_coef',              L2misfit_coef,       ...
             'mesh_perturbation',          mesh_perturbation);


  end


  function value = yesORno(field)
    
  if (strcmp(upper(field), 'YES') == 1) 
    value = 1;
  else
    value = 0;
  end

  end

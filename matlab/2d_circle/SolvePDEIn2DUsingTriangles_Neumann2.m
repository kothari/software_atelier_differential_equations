function [diff, mesh,U, K, M, b, x, L2, H1] = SolvePDEIn2DUsingTriangles_Neumann2

[params]         = readparameters('input.txt');
[mesh]           = MeshCircle(params,10,1,1);
[K,M,b]          = assembleDiscreteOperators(mesh);
[K,b]            = enforceBC(mesh,K,b);
[x]              = solve(K,b);
[U]              = u_exact(mesh.RPhi(:,1), mesh.RPhi(:,2));

plot(x);
hold on;
plot(U);
diff = U-x;
error=norm(diff);
L2 = sqrt((diff)'*M*(diff));
H1 = sqrt((diff)'*M*(diff) + (diff)'*K*(diff));
%writeMeshToVTKFile('output', mesh, mesh.CondactivityE, {'alpha'}, ...
%             [U, x, diff], {'u_exact','u_h','error'}, mesh.VTKtype)
end

%% Make Local Laplacian
function [Ke] = makeKe(e, mesh)

I = mesh.Elements(e, :);
x_e = mesh.Points(I,1);
y_e = mesh.Points(I,2);

x1 = x_e(1);
x2 = x_e(2);
x3 = x_e(3);

y1 = y_e(1);
y2 = y_e(2);
y3 = y_e(3);

T = [ x1 y1 1; ...
    x2 y2 1; ...
    x3 y3 1];

T_invert = inv(T);
V = (1/2)*abs(det(T));

C = T_invert(1:2,:);

Ke = C'*C*V;
Ke = Ke * mesh.CondactivityE(e);

end

%% Make Local Mass
%%%%%%%%%%%%%%%%%%%%
function [Me] = makeMe(e, mesh)

I = mesh.Elements(e, :);
x_e = mesh.Points(I,1);
y_e = mesh.Points(I,2);

x1 = x_e(1);
x2 = x_e(2);
x3 = x_e(3);

y1 = y_e(1);
y2 = y_e(2);
y3 = y_e(3);

T = [ x1 y1 1; ...
      x2 y2 1; ...
      x3 y3 1];

V = (1/2)*abs(det(T));

Me = V/12 * ones(3,3);
%Diagonal
Me(1,1) = V/6;
Me(2,2) = V/6;
Me(3,3) = V/6;

end

%% Assemble Operators
function [K,M,b] = assembleDiscreteOperators(mesh)
N = mesh.NumberOfNodes;
Ne= mesh.NumberOfElements;
Nv= mesh.NumberOfVertices;
M = zeros(N,N);
K = zeros(N,N);
b = zeros(N,1);
for e=1:Ne
    Me = makeMe(e, mesh);
    Ke = makeKe(e, mesh);
    for i = 1:Nv
        I = mesh.Elements(e,i);
        for j = 1:Nv
            J = mesh.Elements(e,j);
            M(I,J) = M(I,J) + Me(i,j);
            K(I,J) = K(I,J) + Ke(i,j);
        end %jloop
    end %iloop
end %eloop
end

%% Enforce Boundary Conditions
function[K,b] = enforceBC(mesh,K,b)
%% Dirichlet
% b(mesh.DirichletIndex) = f(mesh.RPhi(mesh.DirichletIndex,2));
% K(mesh.DirichletIndex,:) = 0;
% for i=1:numel(mesh.DirichletIndex)
%     K(mesh.DirichletIndex(i),mesh.DirichletIndex(i)) = 1;
% end

%% Neumann
gp = zeros(mesh.NumberOfNodes,1);
gp(mesh.DirichletIndex) = g(mesh.RPhi(mesh.DirichletIndex,2));
b_n = mesh.B_N * gp;
b = b - b_n;

K(mesh.DirichletIndex(1),:) = 0;
K(mesh.DirichletIndex(1),mesh.DirichletIndex(1)) = 1;
b(mesh.DirichletIndex(1))=f(mesh.RPhi(mesh.DirichletIndex(1),2));
end

%% Solve PDE
function [x] = solve(K,b)
x = K\b;
end

%% Analytical Solution
function U = u_exact(r, phi)
n = length(r);
U = zeros(n,1);
for i=1:n
    if (r(i) <= 1.0)
        U(i) = 1 + r(i)*cos(phi(i));
    else
        U(i) = 1 + (1.5*r(i) - 0.5/r(i))*cos(phi(i));
    end
    
end
end

%% Dirichlet Condition
function F = f(phi)

% this is the restriction of u_exact on the boundary
F = 1 + 11/4 * cos(phi);

end

%% WriteMeshToVTKFile
%%%%%%%%%%%%%%%%%%%%%%
% write a function that uses writeMeshToVTKFile(...) called postprocessor
% that takes as arguments the name of the file and the solution, and write
% the vtkfile on the disk for visualization with ViSiT
% postprocessor(vtkfile, u);


% function writeMeshToVTKFile(prefix, Mesh, Param_E, labels_E, Param_N, labels_N, type)
%
%     postprocessor(vtkfile, u);
%
% end

%% Neumann cond
function G = g(phi)

% this is the Neumann data
% -grad(u) * n = g
G = -13.0/8.0 * cos(phi);

end
function [error,L2,H1] = SolvePDETriangles_Quadrature
%diff, mesh,U, K, M, b, x, L2, H1
[params]         = readparameters('input.txt');
[mesh]           = MeshCircle(params,40,1,1);
[K,M,b]          = assembleDiscreteOperators(mesh);
[K,b]            = enforceBC(mesh,K,b);
[x]              = solve(K,b);
[U]              = u_exact(mesh.RPhi(:,1), mesh.RPhi(:,2));

plot(x);
hold on;
plot(U);
diff = U-x;
error=norm(diff);
L2 = sqrt((diff)'*M*(diff));
H1 = sqrt((diff)'*M*(diff) + (diff)'*K*(diff));
%writeMeshToVTKFile('output', mesh, mesh.CondactivityE, {'alpha'}, ...
%             [U, x, diff], {'u_exact','u_h','error'}, mesh.VTKtype)
end

%% Make Local Laplacian
function [Ke] = makeKe(e, mesh)
GPoints(:,1)= [4 1 1]/6;
GPoints(:,2)= [1 4 1]/6;
GWeights = [1 1 1]/6;

Nv = mesh.NumberOfVertices;
NumberOfGPoints =size(GPoints,1);

I = mesh.Elements(e, :);
Points = mesh.Points(I,:);

Ke = zeros(Nv,Nv);
for k_GP=1:NumberOfGPoints
    % derivatives of shape functions
    dN =[ -1 -1;
        1  0;
        0  1];
    
    % Jacobian
    Jacobian = dN' * Points;
    detJ = abs(det (Jacobian));
    invJdN = Jacobian\dN';
    gNgN = invJdN'*invJdN;
    Ke = Ke + GWeights(k_GP) * gNgN * detJ;
    
    % for i=1:Nv
    %    for j=1:Nv
    %       Ke(i,j) = Ke(i,j) + (GWeights(k_GP) * ...
    %          (invJdN(1,i)*invJdN(1,j)+invJdN(2,i)*invJdN(2,j))* detJ);
    %    end
    % end
end
Ke = Ke * mesh.CondactivityE(e);
end

%% Make Local Laplacian quadrature points
function [Ke] = makeKequad(e, mesh)
xi = [4 1 1]/6;
eta = [1 4 1]/6;
W = [1 1 1]/6;

I = mesh.Elements(e, :);
Points = mesh.Points(I,:);

% derivatives of shape functions
dN =[ -1 -1;
       1  0;
       0  1];

% Jacobian
Jacobian = dN' * Points;
detJ = abs(det (Jacobian));
invJdN = Jacobian\dN';
invJdN=[invJdN; invJdN; invJdN];
%    Ke = Ke + GWeights(k_GP) * gNgN * detJ;
Ke = mesh.CondactivityE(e)*W.*(invJdN'*invJdN)*detJ;
% 
% for i=1:Nv
%     for j=1:Nv
%         Ke(i,j) = Ke(i,j) + (GWeights(k_GP) * ...
%             (invJdN(1,i)*invJdN(1,j)+invJdN(2,i)*invJdN(2,j))* detJ);
%     end
% end
%Ke = Ke * mesh.CondactivityE(e);
end


%% Make Local Mass
function [Me] = makeMe(e, mesh)
xi= [4 1 1]/6;
eta= [1 4 1]/6;
W = [1 1 1]/6;

I = mesh.Elements(e, :);
Points = mesh.Points(I,:);

% shape functions
N = [1-xi-eta;
    xi;
    eta];

% derivatives of shape functions
dN =[ -1 -1;
    1 0;
    0 1];

%% Jacobian
Jacobian = dN' * Points;
detJ = abs(det (Jacobian));

Me = W.*N*N'*detJ;
end

%% Assemble Operators
function [K1,M,b] = assembleDiscreteOperators(mesh)
N = mesh.NumberOfNodes;
Ne= mesh.NumberOfElements;
M = zeros(N,N);
K = zeros(N,N);
K1 = zeros(N,N);
b = zeros(N,1);
for e = 1:Ne
    Me = makeMe(e, mesh);
    Ke = makeKe(e, mesh);
    Ke1 = makeKequad(e,mesh);
    I = mesh.Elements(e, :) ;
    M(I, I) = M(I, I) + Me;
    K(I, I) = K(I, I) + Ke;
    K1(I, I) = K1(I, I) + Ke1;
end % e loop
norm(K-K1)
end

%% Enforce Boundary Conditions
function[K,b] = enforceBC(mesh,K,b)
%% Dirichlet
% b(mesh.DirichletIndex) = f(mesh.RPhi(mesh.DirichletIndex,2));
% K(mesh.DirichletIndex,:) = 0;
% for i=1:numel(mesh.DirichletIndex)
%     K(mesh.DirichletIndex(i),mesh.DirichletIndex(i)) = 1;
% end

%% Neumann
gp = zeros(mesh.NumberOfNodes,1);
gp(mesh.DirichletIndex) = g(mesh.RPhi(mesh.DirichletIndex,2));
b_n = mesh.B_N * gp;
b = b - b_n;

K(mesh.DirichletIndex(1),:) = 0;
K(mesh.DirichletIndex(1),mesh.DirichletIndex(1)) = 1;
b(mesh.DirichletIndex(1))=f(mesh.RPhi(mesh.DirichletIndex(1),2));
end

%% Solve PDE
function [x] = solve(K,b)
x = K\b;
end

%% Analytical Solution
function U = u_exact(r, phi)
n = length(r);
U = zeros(n,1);
for i=1:n
    if (r(i) <= 1.0)
        U(i) = 1 + r(i)*cos(phi(i));
    else
        U(i) = 1 + (1.5*r(i) - 0.5/r(i))*cos(phi(i));
    end
end
end

%% Dirichlet Condition
function F = f(phi)
% this is the restriction of u_exact on the boundary
F = 1 + 11/4 * cos(phi);
end

%% WriteMeshToVTKFile
%%%%%%%%%%%%%%%%%%%%%%
% write a function that uses writeMeshToVTKFile(...) called postprocessor
% that takes as arguments the name of the file and the solution, and write
% the vtkfile on the disk for visualization with ViSiT
% postprocessor(vtkfile, u);

% function writeMeshToVTKFile(prefix, Mesh, Param_E, labels_E, Param_N, labels_N, type)
%
%     postprocessor(vtkfile, u);
%
% end

%% Neumann cond
function G = g(phi)
% this is the Neumann data
% -grad(u) * n = g
G = -13.0/8.0 * cos(phi);
end
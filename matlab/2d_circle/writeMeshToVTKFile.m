function writeMeshToVTKFile(prefix, Mesh, Param_E, labels_E, Param_N, labels_N, type)
PointList        = Mesh.Points;
ElementList      = Mesh.Elements; 
numberOfPoints   = Mesh.NumberOfNodes;
numberOfElements = Mesh.NumberOfElements;
numberOfVertices = Mesh.NumberOfVertices;

% 2. read the .node file
% %%%%%%%%%%%%%%%%%%%%%%

% this opens a file in text 't mode for read access 'r'
filename = strcat(prefix, '.vtk');

fprintf('writting mesh file %s\n', filename);

fout = fopen(filename, 'w');
fprintf(fout,'# vtk DataFile Version 5.10\n');
fprintf(fout,'Hexahedral mesh with data\n');
fprintf(fout,'ASCII\n');
fprintf(fout,'DATASET UNSTRUCTURED_GRID\n');
fprintf(fout,'POINTS %d float\n', numberOfPoints);


% now write the PointList
% -----------------------


for i = 1:numberOfPoints
    x_i = PointList(i,:);
    fprintf(fout,'%25.16e %25.16e %25.16e\n', x_i(1), x_i(2), 0.0);
end

fprintf(fout,'\n');
entries = (numberOfVertices+1)*numberOfElements;
fprintf(fout,'CELLS %d %d\n', numberOfElements, entries);
first_number = 1;

for e = 1:numberOfElements
    
    v_e = ElementList(e, :);
    
    v_e = v_e - first_number;
    
    fprintf(fout,'%d ', numberOfVertices);
    
    for i=1:numberOfVertices
        fprintf(fout,'%d ', v_e(i));
    end
    
    fprintf(fout, '\n');
    
end


fprintf(fout,'\n');
fprintf(fout,'CELL_TYPES %d\n', numberOfElements);

for e = 1:numberOfElements
    
    fprintf(fout,'%d\n', type);
    
end

fprintf(fout,'\n');


nfields = size(Param_E, 2);

fprintf(fout,'CELL_DATA %d\n', numberOfElements);
for f = 1:nfields
  fprintf(fout,'SCALARS %s float 1\n', labels_E{f});
  fprintf(fout,'LOOKUP_TABLE default\n');
  fprintf( fout,'%25.16e\n', Param_E(:, f) );
end


nfields = size(Param_N, 2)
fprintf(fout,'POINT_DATA %d\n', numberOfPoints);
for f = 1:nfields
  fprintf(fout,'SCALARS %s float 1\n', labels_N{f});
  fprintf(fout,'LOOKUP_TABLE default\n');
  fprintf( fout,'%25.16e\n', Param_N(:, f) );
end



fclose(fout);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Poisson 2D with Quadrature
%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [diff, mesh,U, K, M, b, x, L2, H1] = SolvePDEIn2D_Quadrature

%%%%%%%%%%%%%%%%%%%
%read the parameters
%%%%%%%%%%%%%%%%%%%  
    
    [params]         = readparameters('input.txt');
 
%%%%%%%%%%%%%%%%%%%
%make mesh
%%%%%%%%%%%%%%%%%%%  
    
    [mesh]           = MeshCircle(params,10,1,1);
    
%%%%%%%%%%%%%%%%%%%
%assemble operators
%%%%%%%%%%%%%%%%%%%   
    
    [K,M,b]          = assembleDiscreteOperators(mesh);  
    
%%%%%%%%%%%%%%%%%%%
%enforce BC & make rhs
%%%%%%%%%%%%%%%%%%% 
    [r,phi]          = genrphi(mesh);
    [K,b]            = enforceBC(mesh,K,b,phi);
    
%%%%%%%%%%%%%%%%%%%
%solve PDE
%%%%%%%%%%%%%%%%%%%   

%Estimate

    [K,b,x]        = solve(K,b,mesh);

%Exact

    [U]            = u_exact(r, phi);

plot(x);
hold on;
plot(U);
hold off;
legend('u_h','u_exact')
%%%%%%%%%%%%%%%%%%%
%Convergence Study
%%%%%%%%%%%%%%%%%%%  
%Compute difference
    diff = U-x;    
    error=norm(diff);
    
%L2 Norm
    L2 = sqrt((diff)'*M*(diff));

%H1 Norm
    H1 = sqrt((diff)'*M*(diff) + (diff)'*K*(diff));

end


%%%%%%%%%%%%%%%%%%%%
%Make Local Mass - Quadrature
%%%%%%%%%%%%%%%%%%%%

function [Me] = makeMeQuad(e, mesh)
        I = mesh.Elements(e, :);
        x_e = mesh.Points(I,1);
        y_e = mesh.Points(I,2);
        
        x1 = x_e(1);
        x2 = x_e(2);
        x3 = x_e(3);
        
        y1 = y_e(1);
        y2 = y_e(2);
        y3 = y_e(3);

        T = [ x1 y1; ...
              x2 y2; ...
              x3 y3];
          
%Quadrature Points
    xi =  [4 1 1] / 6;
    eta = [1 4 1] / 6;
    
% Quadrature weights
    W =   [1 1 1] / 6;
    
%Initialize local
    Nv = mesh.NumberOfVertices;
    Me = zeros(Nv, Nv);
    
%Compute Jacobian of Transformation
   
    J = [T(2,1)-T(1,1),T(2,2)-T(1,2);T(2,1)-T(1,1),T(3,1)-T(1,1)];
    
%Compute Determinant of Jacobian
    
    J_det = det(J);

%evaluate the basis function at the Quadrature points
    
    N = [1-xi-eta; xi; eta];
%     N_n = [N;N;N];
%     N_m = N_n*N_n';
%Compute the mass matrix entries over all the Quadrature points

%     Me = W'.*N_n'*N_n*abs(J_det);
for i = 1:3
    for j = 1:3
        Me(i,j) = (W(i) * N(i) * N(j) * abs(J_det));
    end
end

end

%%%%%%%%%%%%%%%%%%%%%%
%Make Local Laplacian - Quadrature
%%%%%%%%%%%%%%%%%%%%%% 

function [Ke] = makeKeQuad(e, mesh)
        
        I = mesh.Elements(e, :);
        x_e = mesh.Points(I,1);
        y_e = mesh.Points(I,2);
        
        x1 = x_e(1);
        x2 = x_e(2);
        x3 = x_e(3);
        
        y1 = y_e(1);
        y2 = y_e(2);
        y3 = y_e(3);

        T = [ x1 y1; ...
              x2 y2; ...
              x3 y3];
        
%Quadrature Points
    xi =  [4 1 1] / 6;
    eta = [1 4 1] / 6;
    
%Quadrature weights
    W =   [1 1 1] / 6;
    
%Initialize local
    Nv = mesh.NumberOfVertices;
    Ke = zeros(Nv, Nv);
    
%Compute Jacobian of Transformation
    J = [T(2,1)-T(1,1),T(2,2)-T(1,2);
        T(2,1)-T(1,1),T(3,1)-T(1,1)];
    
%Compute Determinant of Jacobian
    J_det = det(J);  

%Compute Inverse of Jacobian
    J_inv = inv(J);
    
%Evaluate Basis Functions
    N = [1-xi-eta; xi; eta];
   
%Derivative of Basis Functions
    N_grad = [-1,-1; 1,0; 0,1];
    

%Compute local Laplacian
%Multiply the J_inv(2x2) with the transpose of the gradient of the basis functions(2x3) to
%produce a 2x3 matrix.
   JdN=(J_inv*N_grad');
   %Stack the matrices to form a 6x3 matrix - one 2x3 matrix for each gauss
   %point, and multiply it with itself to produce the inner product terms
   %for Ke
    JdN=[JdN;JdN;JdN];
    JdN_m = JdN'*JdN;
    %Multply each column with the weights and the determinant
 Ke = W.*(JdN_m)*abs(J_det);

%DONT FORGET CONDACTIVITY
 Ke = Ke * mesh.CondactivityE(e);   

end



%%%%%%%%%%%%%%%%%%%%%%
%Assemble Operators
%%%%%%%%%%%%%%%%%%%%%% 

function [K,M,b] = assembleDiscreteOperators(mesh)
    N = mesh.NumberOfNodes;
    Ne= mesh.NumberOfElements;
    Nv= mesh.NumberOfVertices;
    M = zeros(N,N); 
    K = zeros(N,N);
    b = zeros(N,1);
    for e=1:Ne
        Me = makeMeQuad(e, mesh);
        Ke = makeKeQuad(e, mesh);
        for i = 1:Nv
            I = mesh.Elements(e,i);
            for j = 1:Nv
                J = mesh.Elements(e,j);
                M(I,J) = M(I,J) + Me(i,j);
                K(I,J) = K(I,J) + Ke(i,j);
            end %jloop
        end %iloop
    end %eloop


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Generate r and phi for 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[r, phi] = genrphi(mesh)
        r = mesh.RPhi(:,1);
        phi = mesh.RPhi(:,2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Enforce Boundary Conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[K,b] = enforceBC(mesh,K,b,phi)
    
    b_n = zeros(mesh.NumberOfNodes,1);
    gp = zeros(mesh.NumberOfNodes,1);
    for i = 1:length(mesh.DirichletIndex)  
        I = mesh.DirichletIndex(i);
        gp(I) = g(phi(I));
    end
    
    %gp must be negative because of weak formulation and subtraction to put
    %on RHS.
    b_n = mesh.B_N * -gp;
    b = b_n;
   
    %Enforce 1 dirichlet to make solvable
    %hopefully (generally) mesh is organized in a way such that the
    %boundary is indexed as the "highest number nodes"
        K(mesh.NumberOfNodes-1,:) = 0;
        K(mesh.NumberOfNodes-1,mesh.NumberOfNodes-1) = 1;
        b(mesh.NumberOfNodes-1) = f(phi(mesh.NumberOfNodes-1));

end



%%%%%%%%%%%%%%%%%%%%
%Solve PDE
%%%%%%%%%%%%%%%%%%%%

function [K,b,x] = solve(K,b,mesh)
    x = zeros(mesh.NumberOfNodes);
    x = K\b;
end



%%%%%%%%%%%%%%%%%%%%
%Analytical Solution
%%%%%%%%%%%%%%%%%%%%

function U = u_exact(r, phi)
  n = length(r);
  U = zeros(n,1);
  for i=1:n 
    if (r(i) <= 1.0)
      U(i) = 1 + r(i)*cos(phi(i));
    else
      U(i) = 1 + (1.5*r(i) - 0.5/r(i))*cos(phi(i));
    end
  end
end


function F = f(phi)
    % this is the restriction of u_exact on the boundary
    F = 1 + 11/4 * cos(phi);
end

function G = g(phi)
    % this is the Neumann data
    % -grad(u) * n = g
    G = -13.0/8.0 * cos(phi);
end





function Ue = exactsoln1D(Points,exact)
x=Points(:,1);
Ue = exact(x);
end
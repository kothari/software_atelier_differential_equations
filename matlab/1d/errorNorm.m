function [eL_inf,eL_2,eH_1] = errorNorm(Ue,Uh,M,K)
eL_inf   = max(Ue-Uh);
eL_2     = sqrt((Ue-Uh)'*M*(Ue-Uh));
eH_1     = sqrt((Ue-Uh)'*M*(Ue-Uh)+(Ue-Uh)'*K*(Ue-Uh));
end
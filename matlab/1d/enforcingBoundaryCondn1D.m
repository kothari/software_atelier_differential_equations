function[K,M,f] = enforcingBoundaryCondn1D(K,M,f,mesh,exact_sol)

x = mesh.Points(mesh.DirichletPoints,1);
f(mesh.DirichletPoints) = exact_sol(x);
f(:) = f(:) - K(:,mesh.DirichletPoints)*f(mesh.DirichletPoints);

K(mesh.DirichletPoints,:) = 0;

% making diagonal elements 1
tempA = speye(length(K));
tempA(mesh.DirichletPoints,:) = 0;
tempB = speye(length(K));
tempD = tempB-tempA;
K = K + tempD;

M(mesh.DirichletPoints,:)=0;
M(:,mesh.DirichletPoints)=0;

M = M + tempD;

end
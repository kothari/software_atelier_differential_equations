function [K , M , b] = assembleDiscreteOperators(mesh,force)
N   = mesh.NumberOfNodes;
Ne  = mesh.NumberOfElemets;

M = sparse(N, N) ;
K = sparse(N, N) ;
b = zeros(N, 1) ;

for e= 1:Ne
%     nodes  = mesh.Elements(e,:);
%     Points = mesh.Points(nodes,:);
%     fe = makeSource(Points,force);
    
    [Ke,Me,fe] = makeLocalOperators(e,mesh,force);
    I = mesh.Elements(e, :) ;
    M(I, I) = M(I, I) + Me;
    K(I, I) = K(I, I) + Ke;
    b(I) = b(I) + fe;
end % e loop
end

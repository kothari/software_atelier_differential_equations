clear
clc
close all
% number of elements Nel = Test;
% number of elements h = 1. / Nel;
% size of an elements nodes = Nel + 1;
%% test parameters
Test   = 20;          % number of elements in x directions
TestPD = 1;          % polynomial degree of the element

%% setting variables
Nel 	= Test;      % number of elements in x direction
L       = 1;         % length of the domain
h       = L/Nel;     % size of an elements
PDegree = TestPD;

%% lapacian problem
% exact_sol   = @(x) 4*x.*(1-x);
% force       = @(x) 8;

exact_sol   = @(x) sin(pi*x);
force       = @(x) pi^2*sin(pi*x);
%% mesh creation
[mesh] = make1DGrid(Nel,L,PDegree);

%% assembling the operators
[K,M,f] = assembleDiscreteOperators(mesh,force);

%% applying boundary conditions
[Kb,Mb,fb] = enforcingBoundaryCondn1D(K,M,f,mesh,exact_sol);

%% solve the problem
uh=Kb\fb;

% exaxt solution
ue = exactsoln1D(mesh.Points,exact_sol);

% plotting the computed solution
Plotresult1D(mesh.Points,uh);

% error in L2 and H1 norm
[eL_inf,eL2,eH1]=errorNorm(ue,uh,M,K);
tested = [Nel,PDegree];
error = [eL_inf,eL2,eH1];
disp ('   Nel    PDegree')
disp (tested)
disp ('eL_inf       eL2     eH1')
disp (error)
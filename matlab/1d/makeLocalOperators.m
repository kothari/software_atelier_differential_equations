function [Ke,Me,fp] = makeLocalOperators(e,mesh,force)
%% quadrature points and
PD= mesh.PolynomialDegree;
[GaussPoints,GaussWeights] = Gauss_Legendre_quadrature(PD+1,-1,1);

NumberOfGP  = size(GaussPoints,1);
nodes       = mesh.Elements(e,:);
Points      = mesh.Points(nodes);
Nv          = mesh.NumberOfVertices;

Ke = zeros(Nv);
Me = zeros(Nv);
fp = zeros(Nv,1);
N  = zeros(Nv,1);
dN = zeros(Nv,1);

%% loop over all Gauss points
for k_GP=1:NumberOfGP
    xi = GaussPoints(k_GP);
    [N,dN] = evaluateShapeFunctions(xi,PD);
    
    %% Jacobian
    Jacobian = dN' * Points;
    invJ = inv(Jacobian);
    detJ = abs(det (Jacobian));
    
    %% affine transformation
    x = N' * Points;
    fvalue= force(x);
    %% computing m-ass & stiffness matrix, and force vector
    for i=1:Nv
        for j=1:Nv
            Me(i,j) = Me(i,j) + (GaussWeights(k_GP) * N(i) * N(j) * detJ);
            Ke(i,j) = Ke(i,j) + (GaussWeights(k_GP) * invJ *dN(i) * invJ * dN(j) * detJ);
        end
        fp(i) = fp(i) + (GaussWeights(k_GP)* fvalue * N(i) * Jacobian);
    end    
end
end
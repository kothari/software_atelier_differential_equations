function [Ni,dNi] = shapeFunctions1D(NP)
syms xi;
sil=linspace(-1,1,NP);
for i=1:length(sil)
    num = 1;
    den = 1;
    for j=1:length(sil)
        if(i~=j)
            num=num.*(xi-sil(j));
            den=den.*(sil(i)-sil(j));
        end
    end
    Ni(i,1)=num./den;
end
Ni=expand(Ni);
dNi=diff(Ni,'xi');
end
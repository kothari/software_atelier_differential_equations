function [mesh] = make1DGrid(Nel,L,PDegree)
% details of the grid
dx = L/Nel;
dimension          = 1;
NumberOfElements   = Nel;
PolynomialDegree   = PDegree;
NumberOfVertices   = PolynomialDegree+1;
NumberOfNodes      = (Nel * PolynomialDegree) + 1;

nodes           = zeros(NumberOfNodes,dimension);
elements        = zeros(NumberOfElements,NumberOfVertices);
PointMarkers    = zeros(NumberOfNodes,1);

%% list of nodes and marking the boundary conditions
for i=1:NumberOfNodes
    px = (i-1)*dx/(NumberOfVertices-1);
    nodes(i,:) = px;
end

%%
%   |   i    |  i+1   |  i+2   |
%   |________|________|________|
%  j       j+1      j+2      j+3
%

%% Elements
v = zeros(1,NumberOfVertices);
for i=1:Nel
    e  = i;
    for j=1:NumberOfVertices
        v(j) = (NumberOfVertices-1)*(i-1)+j;
    end
    elements(e,:)=v;
end

%% Boundary points
PointMarkers(1)=1;
PointMarkers(NumberOfNodes)=1;

DirichletPoints=find(PointMarkers);
mesh = struct('h_x', dx, 'PolynomialDegree', PolynomialDegree, ...
    'NumberOfNodes', NumberOfNodes, 'L_x', L, ...
    'NumberOfElemets', NumberOfElements, 'NumberOfVertices', NumberOfVertices,...
    'Elements', elements, 'Points', nodes, 'PointMarkers', PointMarkers,...
    'DirichletPoints', DirichletPoints, 'u_h', []);
end
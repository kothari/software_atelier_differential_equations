function M = Mass1DSymbolicP1()

x  = sym('x' , 'real'); 
x1 = sym('x1', 'real'); 
x2 = sym('x2', 'real');

%length of the element
h = sym('h', 'real'); 

%basis functions
phi(1) = (x-x1)/(x2-x1);
phi(2) = (x2-x)/(x2-x1);

%replace x2-x1 as h
%ansphi=subs(phi,x2-x1,h);
grad_phi=diff(phi,x);

F = phi'*phi;

%integration
M = int(F, 'x', x1, x2); 

%replace x2-x1 as h
M = subs(M,x2-x1,h)
end

#/bin/bash
if [ $# -ne 3 ]; then
  echo "usage: $0 color 'rgb(0,0,0)' input_file outfile"
  exit 1
fi

cp $2 $2.bak
convert $2 -fuzz 10% -fill 'rgb(245,245,245)' -opaque $1 $3

\documentclass[10pt]{beamer}

\usetheme{ICS}
\usefonttheme{professionalfonts}

\usefonttheme[stillsansseriflarge]{serif}
%\setbeamerfont{frametitle}{series} % Frame titles should be bold
\usepackage{biblatex}
\usepackage[utf8]{inputenc} 

\usepackage{multimedia}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{algorithm}
%\usepackage[]{algorithmic}
\usepackage{marvosym}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage[format=hang,font+=scriptsize,labelfont+=scriptsize]{caption}
%\usepackage[font+=scriptsize,labelfont+=scriptsize,format=hang]{subcaption}
\captionsetup{compatibility=false}
\usepackage{wasysym}
%\usepackage[draft]{movie15}
\usepackage{pifont}
\usepackage{array}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{pgfplotstable}
\usepackage{pgfplots}
\tikzset{%
    dimen/.style={<->,>=latex,thin,every rectangle node/.style={fill=white,midway,font=\sffamily}}
}
\usetikzlibrary{arrows}
\newcommand{\bs}{\boldsymbol}
\newcommand{\bx}{\boldsymbol{x}}
\newcommand{\bu}{\boldsymbol{u}}
\newcommand{\bv}{\boldsymbol{v}}
\definecolor{checkgreen}{rgb}{0,0.6,0}
\definecolor{phase1}{rgb}{0.008,0.655,1.000}
\definecolor{phase2}{rgb}{0.016,0.75,0.700}
\definecolor{phase3}{rgb}{0.929,0.35,0.700}
\definecolor{icsyellow}{cmyk}{0.00,0.11,0.53,0.00}

\newcommand{\ok}{\textcolor{green}{\checked}}
\newcommand{\pro}{\textcolor{checkgreen}{\ding{51}}}
\newcommand{\con}{\textcolor{red}{\ding{55}}}

\renewcommand\thesubfigure{\alph{subfigure}}

\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip2em
  \hbox{}\nobreak \rm \hfil(#1)%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}
\newsavebox\mybox
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}}
  {\signed{\usebox\mybox}\end{quote}}

% Ausschalten der kleinen Navigationselemente
\beamertemplatenavigationsymbolsempty

\newcommand{\Tt}[1]{\mathbf{#1}}
\newcommand{\vnorm}[1]{\left|\left|#1\right|\right|}
\newtheorem{proposition}{Proposition}

\title[Software Ateliear: Partial Differntial Equations]
{
  Software Ateliear: Partial Differential Equations
}
\subtitle{Introduction to FreeFEM++}
\author[D. Kourounis, H. Kothari]
{
  Drosos Kourounis, Hardik Kothari
}

\institute
{
Institute of Computational Science \\ Universit{\`a} della Svizzera italiana \\ Lugano, Switzerland
}

\date[21.09.2017]
{
  September 21, 2017
}


\begin{document}
\lstset{language=C,
        basicstyle=\ttfamily\scriptsize,
        commentstyle=\color{blue}\ttfamily,
        frame=single,
        rulecolor=\color{black},    %%% <--- here
        breaklines=true,
        showstringspaces=false,
}

\renewcommand{\pgfuseimage}[1]{\scalebox{.75}{\includegraphics{#1}}}

\frame{
  \titlepage
}
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
\section{Overview}
\begin{frame}
  \frametitle{MATLAB example}
  \begin{itemize}
    \item one-dimensional FEM code for solving Poisson equation
    \item works with higher order elements
    \item higher order integration rules
    \item plotting solution
    \item error analysis
    \item get the code from bitbucket
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{How to install}
  \begin{itemize}
      \item go to the FreeFem++ website: http://www.freefem.org/ff++/
      \item For Mac Download and install  FreeFem++-3.56-MacOS\_10.12.pkg 
      \item follow the downloading steps
  \end{itemize}

      After the installation finishes
  \begin{itemize}
      \item The binaries can be found at /usr/local/bin
      \item FreeFem++-CoCoa
      \item the full package is installed at /usr/local/ff++/openmpi-2.1/3.56
      \item if you can not find FreeFem++ binary
      \item export PATH=\$PATH:/usr/local/ff++/openmpi-2.1/3.56/bin
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Recipe for FEM}
  \begin{itemize}
    \item Generate the mesh
    \item Define the finite element space
    \item Define the variational problem
    \item Assemble the variational problem 
    \item Solve the linear system of equation
    \item Post-processing or visualization of the solution
  \end{itemize}
\end{frame}
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
\section{Laplace equation}
\begin{frame}
  Strong formulation:
  \begin{equation*} \label{eq:strong_form}
    \begin{aligned}
      -\nabla \cdot \nabla u & = f \quad \text{ in } \Omega \\
      u                      & = g \quad \text{ on } \Gamma_D\\
      \nabla u \cdot n       & = h \quad \text{ on } \Gamma_N
    \end{aligned}
  \end{equation*}
  Weak Formulation:\\
  Find $u \in \mathcal{V}$ such that
  \begin{equation*} \label{eq:weak_form}
    \int_{\Omega} \nabla u \cdot \nabla v d\Omega = \int_{\Omega} f v\
    d\Omega-\int_{\Gamma_N} hv \ d\Gamma_N \quad \forall v\in \mathcal{\hat{V}}
  \end{equation*}
  Trial space is defined by
  \begin{equation*}
    \mathcal {V}  = \{v \in H^\Omega): v = g \text{ on } \Gamma_D \}
  \end{equation*}
  and the test space is defined by
  \begin{equation*}
    \mathcal {\hat{V}}  = \{v \in H^1(
      (\Omega): v = 0 \text{ on } \Gamma_D \}
  \end{equation*}
\end{frame}

\begin{frame}
  \frametitle{Finite dimensional discretization}
  The next step would be to discretize the weak formulation by restriction the
  variational problem to discrete spaces\\
  Find $u_h \in \mathcal{V}_h \subset \mathcal{V}$ such that
  \begin{equation*} \label{eq:weak_form}
    \int_{\Omega} \nabla u_h \cdot \nabla v d\Omega = \int_{\Omega} f v\
    d\Omega-\int_{\Gamma_N} hv \ d\Gamma_N \quad \forall v\in \mathcal{\hat{V}}_h
    \subset \mathcal{\hat{V}}
  \end{equation*}
  Bilinear form:
  \begin{equation*} 
    a(u_h,v) := \int_{\Omega} \nabla u_h \cdot \nabla v d\Omega
  \end{equation*}
  Linear form:
  \begin{equation*} 
    L(v) :=\int_{\Omega} f v\ d\Omega-\int_{\Gamma_N} hv \ d\Gamma_N 
  \end{equation*}
\end{frame}

\begin{frame}[fragile]
\begin{lstlisting}[frame=single,breaklines]
// create the mesh
mesh Th=square(10,10);

//associate Finite element space with the Mesh Th
fespace Vh(Th,P1);

// Define the trial and test function on the FE space
Vh uh,vh;

// force or source function
func f=1;

// Boundary condition
func g=0;

// weak formulation of the problem
//problem laplace(uh,vh,solver=GMRES,tgv=1e5) =
problem laplace(uh,vh) =
int2d(Th)( dx(uh)*dx(vh) + dy(uh)*dy(vh)  ) // bilinear form
- int2d(Th)( f*vh  )                        // linear form
+ on(1,2,3,4,uh=g) ;                        // boundary condition form


// solve the problem
laplace;

// visualize the result
plot(uh);
\end{lstlisting}
\end{frame}

%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
\section{Heat equation}
\begin{frame}
  Strong formulation:
  \begin{equation*}
    \begin{aligned}
      \frac{\partial u}{\partial t}   -\nabla \cdot (\kappa\ \nabla u) & = 0 & \text{ in } [0,T] \times \Omega \\
      u                      & = u_0   & \text{ in } 0 \times \Omega \\
      u                      & = u_D   & \text{ in } [0,T] \times \Gamma_D\\
      \nabla u \cdot n       & = u_N   & \text{ in } [0,T] \times \Gamma_N 
    \end{aligned}
  \end{equation*}

  Time discretization with finite difference scheme:
  \begin{equation*}
    \begin{aligned}
    \frac{\partial u}{\partial t} & \approx \frac{u^{n+1}-u^n}{\Delta t} \\
                                  & = \theta F(u^{n+1}) + (1-\theta) F(u^n)
    \end{aligned}
  \end{equation*}
  $\theta$- scheme
  \begin{itemize}
    \item Forward Euler scheme ($\theta = 0$)
    \item Backward Euler scheme ($\theta = 1$)
    \item Crank-Nicholson scheme ($\theta = \frac{1}{2}$)
  \end{itemize}

\end{frame}


\begin{frame}
  Using Backward Euler scheme:
  \begin{equation*}
      \frac{u^{n+1}-u^{n}}{\Delta t} -\nabla \cdot (\kappa\ \nabla u^{n+1}) = 0
  \end{equation*}
  
  Weak formulation:
  \begin{equation*}
  \int_\Omega \frac{u^{n+1}}{\Delta t} v\ d\Omega - \int_\Omega
  \frac{u^n}{\Delta t} v\ d\Omega + \int_{\Omega} \kappa\ (\nabla u^{n+1} \nabla
  v) - \int_{\Gamma_N}\kappa (u_N v) \ d\Gamma_N= 0
  \end{equation*}

Find $u^{n+1} \in \mathcal{V}$ such that 
  \begin{equation*}
    a(u,v) = L(v) \quad \forall v \in \mathcal{\hat{V}}
  \end{equation*}
with
 \begin{equation*}
   \begin{aligned}
     a(u,v) &= \int_\Omega \frac{u^{n+1}}{\Delta t} v\ d\Omega + \int_{\Omega}
     \kappa\ (\nabla u^{n+1} \nabla v) \ d \Omega \\    
  L(v) &= - \int_\Omega \frac{u^n}{\Delta t} v\ d\Omega - \int_{\Gamma_N}\kappa (u_N v) \ d\Gamma_N
   \end{aligned}
 \end{equation*}


\end{frame}



\begin{frame}[fragile]
\begin{lstlisting}[frame=single,breaklines]
// time loop
int t;

// number of iterations
int iter=10;

// time step
real dt=0.01;

// create the mesh
real x0=0, x1=1;
real y0=0, y1=1;

int nn = 10;

// create the mesh
mesh Th=square(nn,nn,[x0+(x1-x0)*x,y0+(y1-y0)*y]);

// show how the mesh looks like
plot(Th,wait=1,cmm="square mesh");

//associate Finite element space with the Mesh Th
fespace Vh(Th,P2);

// Define the trial and test function on the FE space
Vh uh,u0,vh;

// initial condition
uh = sin(pi*x)*sin(pi*y);
\end{lstlisting}
\end{frame}



\begin{frame}[fragile]
\begin{lstlisting}[frame=single,breaklines]
// force or source function
func f=0;

// Boundary condition
func g=0;

// weak formulation of the problem
real kappa = 1;

// problem
problem heat(uh,vh,init=t)
          // bilinear form
          = int2d(Th)(uh*vh /dt)
          + int2d(Th)( kappa*(dx(uh)*dx(vh)+dy(uh)*dy(vh)) )
          // linear form
          - int2d(Th)(u0*vh/dt)
          // boundary condition form
          + on(1,2,3,4,uh=g) ;


// solve the problem
for (t=0 ; t<iter ; t++ ){
  // update from previous step
  u0=uh;
  // solve linear problem
  heat;

  // visualize the result
  plot(uh,wait=1,fill=1,value=1,cmm="solution at time "+t*dt);
}

\end{lstlisting}
\end{frame}


%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%
\section{Linear Elasticity}
\begin{frame}
  Balance law:
  \begin{equation*} 
    -\nabla \cdot \sigma = f
  \end{equation*}
  Strain-displacement relationship:
  \begin{equation*} 
    \varepsilon(u) = \frac{1}{2} (\nabla u + \nabla u^T) 
  \end{equation*}
  Constitutive law:
  \begin{equation*} 
    \sigma = 2\mu \varepsilon(u) + \lambda(\nabla \cdot u)I
  \end{equation*}
  combining above equations:
  \begin{equation*} 
  -\nabla \cdot [2 \mu \varepsilon(u) + \lambda(\nabla \cdot u)I ]  = f
  \end{equation*}
Boundary conditions:
  \begin{equation*} 
    \begin{aligned}
      u &= u_0 & \text{ on } \Gamma_D \\
      \sigma \cdot n &= t & \text{ on } \Gamma_N 
    \end{aligned}
  \end{equation*}

\end{frame}

\begin{frame}
  Weak formulation:
  \begin{equation*} 
    \int_{\Omega} (-\nabla \cdot \sigma - f) \cdot v \ d\Omega= 0
  \end{equation*}
  Using Green's formula
  \begin{equation*} 
    \int_{\Omega} \sigma \cdot \nabla v \ d\Omega =  \int_{\Omega} f\cdot v \
    d\Omega + \int_{\Gamma_N} t\cdot v \ d\Gamma_N
  \end{equation*}

  Substituting the values for stress and traction
  \begin{equation*} 
    \int_{\Omega} [2\mu \varepsilon(u) + \lambda(\nabla \cdot u)I ] \cdot \nabla v \ d\Omega =  \int_{\Omega} f\cdot v \ d\Omega + \int_{\Gamma_N} t\cdot v \ d\Gamma_N
  \end{equation*}
  
  \begin{equation*} 
    \int_{\Omega} {2\mu}(\varepsilon(u):\varepsilon(v)) \ d\Omega + \int_\Omega \lambda(\nabla \cdot u)(\nabla \cdot v)\ d\Omega =  \int_{\Omega} v\cdot f \
    d\Omega + \int_{\Gamma_N} v\cdot t \ d\Gamma_N
  \end{equation*}

\end{frame}


\begin{frame}
  Weak formulation:\\
  Find $u \in \mathcal{V}$ such that:
  \begin{equation*}
    a(u,v) = L(v) \quad \forall v \in \mathcal{\hat{V}}
  \end{equation*}
with
 \begin{equation*}
   \begin{aligned}
     a(u,v) &= \int_{\Omega} {2\mu}(\varepsilon(u):\varepsilon(v)) \ d\Omega + \int_\Omega \lambda (\nabla \cdot u)(\nabla \cdot v)\
     d\Omega \\
     L(v) &=  \int_{\Omega} f\cdot v \ d\Omega + \int_{\Gamma_N} t\cdot v \ d\Gamma_N
   \end{aligned}
 \end{equation*}
\end{frame}


\begin{frame}[fragile]
\begin{lstlisting}[frame=single,breaklines]
// create the mesh
mesh Th=square(10,10);

// here we have unknown displacement field u,
// with two components, in X and Y directions
// associate Finite element space with the Mesh Th
fespace Vh(Th,[P1,P1]);

// meterial properties
real mu=1, lambda=1;

// Define the trial and test function on the FE space
Vh [u1,u2],[v1,v2];

// the functions for boundary conditions
func g1 = 0;
func g2 = -1;

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\begin{lstlisting}[frame=single,breaklines]
// define macro for the divergence operator
macro div(u1,u2) (dx(u1)+dy(u2)) // EOM

// define macro for Strain-displacement relationship 
macro epsilon(u1,u2) [ dx(u1),dy(u2),(dy(u1)+dx(u2))/sqrt(2) ] // EOM

// define the problem a(u,v)-L(v)=0
problem elasticity([u1,u2],[v1,v2])=
                      // bilinear from
                      int2d(Th)(lambda*div(u1,u2)*div(v1,v2) 
                      + 2.*mu* (epsilon(u1,u2)'*epsilon(v1,v2)))
                      - int1d(Th,2)(g1*v1 + g2*v2) 
                      + on(4,u1=0,u2=0);

// solve the problem
elasticity;

// plot the solution
plot([u1,u2],wait=1);

// import vtk input-output to visualize in paraview
load "iovtk"
savevtk("elasticity.vtk",Th,[u1,u2]);

\end{lstlisting}
\end{frame}


\end{document}
